#php_aes

## 介绍
基于openssl的aes-256-cbc加解密的小脚本

## 用法
```shell script
# 查看所有用法
$ php Aes.php -h
  用法 : php Aes.php [命令] ...
  用AES-256-CBC做加解密
  命令列表 :
  -h --help                              帮助
  -s <密匙>        --secret <密匙>       加解密数据需要的密匙
  -d <加密数据>    --decrypt <加密数据>  解密数据
  -e <原数据>      --encrypt <原数据>    加密数据
```

## 结语
要用的自取吧，同时，在这里欢迎各路大神前来交流！
