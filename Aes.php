<?php

class Aes
{
    public $aesMethod = 'AES-256-CBC';
    public $aesOption = 0;
    public $aesIv;

    public function __construct()
    {
        $this->aesIv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->aesMethod));
    }

    public function encrypt($data, $secret)
    {
        $encrypted = openssl_encrypt($data, $this->aesMethod, $secret, $this->aesOption, $this->aesIv);
        return base64_encode($encrypted . '::' . $this->aesIv);
    }

    public function decrypt($data, $secret)
    {
        $arr = explode('::', base64_decode($data));
        $encrypted = $arr[0];
        $iv = $arr[1];
        return openssl_decrypt($encrypted, $this->aesMethod, $secret, $this->aesOption, $iv);
    }
}

class ShellItem
{
    public $argvOption;
    public $argvData;

    public function __construct($argvOption, $argvData = '')
    {
        $this->argvOption = $argvOption;
        $this->argvData = $argvData;
    }
}

class ShellOperation
{
    public static function searchArgv($argv, $argvName, $returnData = false)
    {
        $argvIdx = 0;
        if (is_array($argvName)) {
            foreach ($argv as $argvOption) {
                if (in_array($argvOption, $argvName)) {
                    if ($returnData) {
                        $argvIdx++;
                        if (!isset($argv[$argvIdx])) {
                            return false;
                        }
                        return new ShellItem($argvOption, $argv[$argvIdx]);
                    }
                    return new ShellItem($argvOption);
                }
                $argvIdx++;
            }
            return false;
        }
        foreach ($argv as $argvOption) {
            if ($argvOption == $argvName) {
                if ($returnData) {
                    $argvIdx++;
                    if (!isset($argv[$argvIdx])) {
                        return false;
                    }
                    return new ShellItem($argvOption, $argv[$argvIdx]);
                }
                return new ShellItem($argvOption);
            }
            $argvIdx++;
        }
        return false;
    }
}

interface ArgvOperation
{
    public function exec();
}

class HelpOperation implements ArgvOperation
{
    const helpMsg = [
        '-h' => '',
        '--help' => "                             帮助" . PHP_EOL,
        '-s' => "<密匙>        ",
        '--secret' => "<密匙>       加解密数据需要的密匙" . PHP_EOL,
        '-d' => "<加密数据>    ",
        '--decrypt' => "<加密数据>  解密数据" . PHP_EOL,
        '-e' => "<原数据>      ",
        '--encrypt' => "<原数据>    加密数据" . PHP_EOL
    ];

    public function exec()
    {
        static::showHelp();
    }

    public static function showHelp()
    {
        echo '用法 : php ' . str_replace(__DIR__ . '/', '', __FILE__) . ' [命令] ...' . PHP_EOL;
        echo '用AES-256-CBC做加解密' . PHP_EOL;
        echo '命令列表 : ' . PHP_EOL;
        foreach (static::helpMsg as $operation => $msg) {
            echo $operation . ' ' . $msg;
        }
    }
}

class EncryptOperation implements ArgvOperation
{
    protected $argv;

    public function __construct($argv)
    {
        $this->argv = $argv;
    }

    public function exec()
    {
        $shellItem = ShellOperation::searchArgv($this->argv, ['-e', '--encrypt'], true);
        if (!$shellItem) {
            echo '缺少参数！格式应为 -e|--encrypt <原数据>' . PHP_EOL;
            HelpOperation::showHelp();
            return;
        }

        $secretItem = ShellOperation::searchArgv($this->argv, ['-s', '--secret'], true);
        if (!$secretItem) {
            echo '缺少密匙！应加上 -s|--secret <密匙>' . PHP_EOL;
            HelpOperation::showHelp();
            return;
        }

        $aes = new Aes();
        $encrypted = $aes->encrypt($shellItem->argvData, $secretItem->argvData);
        echo $encrypted . "\n";
    }
}

class DecryptOperation implements ArgvOperation
{
    protected $argv;

    public function __construct($argv)
    {
        $this->argv = $argv;
    }

    public function exec()
    {
        $shellItem = ShellOperation::searchArgv($this->argv, ['-d', '--decrypt'], true);
        if (!$shellItem) {
            echo '缺少参数！格式应为 -d|--decrypt <加密数据>' . PHP_EOL;
            HelpOperation::showHelp();
            return;
        }

        $secretItem = ShellOperation::searchArgv($this->argv, ['-s', '--secret'], true);
        if (!$secretItem) {
            echo '缺少密匙！应加上 -s|--secret <密匙>' . PHP_EOL;
            HelpOperation::showHelp();
            return;
        }

        $aes = new Aes();
        $decrypted = $aes->decrypt($shellItem->argvData, $secretItem->argvData);
        echo $decrypted . "\n";
    }
}

if ($argc > 1) {
    foreach ($argv as $operation) {
        switch ($operation) {
            case '-h':
            case '--help':
                $op = new HelpOperation();
                $op->exec();
                continue 2;
            case '-e':
            case '--encrypt':
                $op = new EncryptOperation($argv);
                $op->exec();
                continue 2;
            case '-d':
            case '--decrypt':
                $op = new DecryptOperation($argv);
                $op->exec();
                continue 2;
            default:
                continue 2;
        }
    }
} else {
    HelpOperation::showHelp();
}
